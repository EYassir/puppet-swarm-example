#!/bin/bash -x

if [[ $(docker service ls | grep -c monservice_puppet) == "1" ]]; then
    docker service update \
            --image eyassir/puppettestdocker \
            monservice_puppet
else
    docker service create \
            --name monservice_puppet \
            --publish published=5000,target=5000 \
            eyassir/puppettestdocker
fi